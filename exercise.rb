class Exercise

  # Assume that "str" is a sequence of words separated by spaces.
  # Return a string in which every word in "str" that exceeds 4 characters is replaced with "marklar".
  # If the word being replaced has a capital first letter, it should instead be replaced with "Marklar".
  def self.marklar(str)
    string_array = str.scan(/[\w']+/)
    string_array.each do |word|
      letter = word[0,1] 
      if letter == letter.upcase and word.length > 4 
        str = str.sub(word, "Marklar")
      elsif word.length > 4
        str = str.sub(word, "marklar")
      end
      
    end
    return str
  end

  # Return the sum of all even numbers in the Fibonacci sequence, up to
  # the "nth" term in the sequence
  # eg. the Fibonacci sequence up to 6 terms is (1, 1, 2, 3, 5, 8),
  # and the sum of its even numbers is (2 + 8) = 10
  def self.even_fibonacci(nth)
    current_term, results, first, second, current = 2, 0, 1, 2, 2

    while current_term < nth
      if current % 2 == 0
        results += current
      end
      current = first + second
      first, second = second, current
      current_term += 1
    end

    return results
  end
end

